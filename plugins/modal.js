const modal = document.createElement('div')

modal.id = 'modal'
modal.classList.add('modal')

modal.insertAdjacentHTML(
   'afterbegin',
   `
<div class="modal-overlay" data-close="true">
   <div class="modal-window">
      <div class="modal-header">
         <span class="modal-title">User details</span>
         <span class="modal-close" data-close="true">&times;</span>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
      <button id="modalBtn" class="button delete-user">Delete user</button>
      </div>
   </div>
</div>
`
)

function closeModal() {
   modal.classList.remove('open')
   modal.classList.add('hide')
   setTimeout(() => {
      modal.classList.remove('hide')
      document.getElementsByClassName('modal-body')[0].innerHTML = ''
      document.body.removeChild(modal)
   }, 200)
}

modal.addEventListener('click', event => {
   if (event.target.dataset.close) closeModal()
})
