const config = {
   usersAPI: 'https://jsonplaceholder.typicode.com/users',
   lastUserId: 0 //have to keep it as done have access to db
}

let users = []

const create = element => document.createElement(element)

const createTableCell = data => {
   let cell = create('td')
   cell.innerText = data
   return cell
}

const createTableRow = data => {
   let row = create('tr')
   row.id = `user_${data.id}`
   row.appendChild(createTableCell(data.name))
   row.appendChild(createTableCell(data.username))
   row.appendChild(createTableCell(data.email))
   row.appendChild(createTableCell(data.website))
   tbody.appendChild(row)
}

function redrawTable() {
   tbody.innerHTML = ''
   users.forEach(user => {
      createTableRow(user)
   })
}

function parseNdrawObject(obj, options, nestingLvl = 0) {
   let content = create('div')
   content.style.color = options.color[nestingLvl] || 'black'
   content.style.fontSize = `${1.2 - 0.2 * nestingLvl}rem`
   Object.entries(obj).forEach(element => {
      if (element[0] !== 'id') {
         let record = create('div')
         record.style.marginBottom = '5px'
         let title = create('i')
         title.style.marginLeft = `${2 + nestingLvl}rem`
         title.innerHTML = element[0] + ': '
         record.appendChild(title)
         if (typeof element[1] == 'string') {
            let value = create('strong')
            value.innerHTML = element[1]
            record.appendChild(value)
         } else {
            deeper = nestingLvl + 1
            record.appendChild(parseNdrawObject(element[1], options, deeper))
         }
         content.appendChild(record)
      }
   })
   return content
}

//*============== User template should be taken elsewhere ================
//*================ meanwhile parsing existing one as template ============
function parseObjectAsTemplate(obj, preName = 'user', nestingLvl = 0) {
   let content = create('div')
   Object.entries(obj).forEach(element => {
      if (element[0] !== 'id') {
         let record = create('div')
         record.style.marginBottom = '5px'
         if (typeof element[1] == 'string') {
            let title = create('i')
            title.style.marginLeft = `${2 + nestingLvl}rem`
            title.innerHTML = element[0] + ': '
            record.appendChild(title)
            let input = create('input')
            input.id = `${preName}_${element[0]}`
            input.value = ''
            record.appendChild(input)
         } else {
            let title = create('i')
            title.style.marginLeft = `${2 + nestingLvl}rem`
            title.innerHTML = element[0] + ': '
            record.appendChild(title)
            deeper = nestingLvl + 1
            nestedId = preName + '_' + element[0]
            record.appendChild(parseObjectAsTemplate(element[1], nestedId, deeper))
         }
         content.appendChild(record)
      }
   })
   return content
}

function removeUser(id) {
   const filtered = users.filter(user => user.id != id)
   users = [...filtered]
   redrawTable()
}

//*================= fetching DELETE user =================
// function removeUser(id) {
//    closeModal()
//    fetch(config.usersAPI + '/' + id, {
//       method: 'DELETE'
//    })
//       .then(() => {
//          const filtered = users.filter(user => user.id != id)
//          users = [...filtered]
//          redrawTable()
//       })
//       .catch(() => alert('Something went wrong'))
// }

function parseById(obj, key, value) {
   if (!(key.indexOf('_') + 1)) obj[key] = value
   else {
      let innerKey = key.substring(key.indexOf('_') + 1, key.length)
      key = key.substring(0, key.indexOf('_'))
      if (!(key in obj)) {
         obj[key] = {}
      }
      parseById(obj[key], innerKey, value)
   }
}

function addUser(elements) {
   let newUser = {}
   config.lastUserId++
   newUser.id = config.lastUserId //have to add id myself  for now as db will not do it for me
   for (let element of elements) {
      let key = element.id.substring(element.id.indexOf('_') + 1, element.id.length)
      parseById(newUser, key, element.value)
   }
   users.push(newUser)
   redrawTable()
   //====== fetching POST new user =========
   // fetch(config.usersAPI, {
   //          method: 'POST',
   //          headers: {},
   //          body: JSON.stringify(newUser)
   //       })
   //       .then(res => res.json())
   //       .then(data => users.push(data))
   //       .catch(() => alert('Something went wrong'))
}

//*===================== init fetch =======================
;(async function fetchUsers(apiURL) {
   try {
      let response = await fetch(apiURL)
      let data = await response.json()
      data.forEach(element => {
         users.push(element)
         createTableRow(element)
         config.lastUserId = element.id
      })
   } catch (e) {
      alert('Something went wrong')
   }
})(config.usersAPI)

//*====================== sorting ===========================
thead.onclick = e => {
   function byField(field) {
      return (a, b) => (a[field] > b[field] ? 1 : -1)
   }
   let field = e.target.innerText.toLowerCase()
   users.sort(byField(field))
   redrawTable()
}

//*=============== showing User info in modal================
tbody.onclick = e => {
   const id = e.target.parentNode.id.substring(5)
   for (let user of users) {
      if (user.id == id) {
         document.body.prepend(modal)
         modalBtn.onclick = e => {
            closeModal()
            removeUser(id)
         }
         modalBtn.innerText = 'Delete user'
         let content = parseNdrawObject(user, { color: ['#000', '#4c4c4c', '8c8c8c'] })
         document.getElementsByClassName('modal-body')[0].appendChild(content)
         setTimeout(() => {
            modal.classList.add('open')
         }, 0)
      }
   }
}

//*================== calling modal with add user template ===============
addBtn.onclick = e => {
   document.body.prepend(modal)
   let form = create('form')
   form.id = 'addUserForm'
   let content = parseObjectAsTemplate(users[0])
   form.appendChild(content)
   document.getElementsByClassName('modal-body')[0].appendChild(form)
   modalBtn.onclick = e => {
      if (user_name.value && user_username.value && user_email.value && user_website.value) {
         closeModal()
         addUser(addUserForm.elements)
      } else alert('Please fill at least name, username, email and website')
   }
   modalBtn.innerText = 'Add user'
   setTimeout(() => {
      modal.classList.add('open')
   }, 0)
}
